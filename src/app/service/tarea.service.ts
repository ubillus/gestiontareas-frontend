import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GLOBAL } from './global';
import { Tarea } from '../../model/tarea';

@Injectable({
  providedIn: 'root'
})
export class TareaService {

  public url:string;

  constructor(private _http:HttpClient) { 
      this.url=GLOBAL.url;
  }

  getListaTarea(){
    return this._http.get(this.url+"/listaTarea");
  }

  save(tarea:Tarea){
    let json = JSON.stringify(tarea);
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    return this._http.post(this.url+"/save",json,{headers:headers});
  }

  update(tarea:Tarea){
    let json = JSON.stringify(tarea);
    let headers = new HttpHeaders({'Content-Type':'application/json'});
    return this._http.put(this.url+"/update",json,{headers:headers});
  }

  delete(id:number){
    return this._http.delete(this.url+"/delete/"+id);
  }
}
