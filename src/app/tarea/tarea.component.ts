import { Component, OnInit } from '@angular/core';
import { Tarea } from '../../model/tarea';
import { TareaService } from '../service/tarea.service';
import { MessageService,Message } from 'primeng/api';

@Component({
  selector: 'app-tarea',
  templateUrl: './tarea.component.html',
  styleUrls: ['./tarea.component.css']
})
export class TareaComponent implements OnInit {

  msgs:Message[]=[];
  idSelected:number;
  tareas : Tarea[];
  cols: any[];
  clonedTarea: { 
    [s: string]: Tarea; 
  } = {};
  tarea:Tarea = {
    id:null,
    descripcion:null,
    estado:null
  };

  constructor(private tareaService : TareaService,private messageService: MessageService) { }

  //Listar todas las tareas
  getTareas(){
    this.tareaService.getListaTarea().subscribe(
      (result:any)=>{
        let tareas : Tarea[] = [];
        for(let i=0;i<result.length;i++){
          let tarea = result[i] as Tarea;
          tareas.push(tarea);
        }
        this.tareas = tareas;
      },error=>{
        console.log(error);
      }
    );
  }

  //Registra una tarea nueva
  save(){
    if(this.tarea.descripcion==null || this.tarea.descripcion.trim()==""){
      this.msgs = [];
      this.msgs.push({severity:'warn', summary:'Advertencia', detail:'Completar los campos'});
    }else{
      this.msgs=[];
      this.tareaService.save(this.tarea).subscribe(
        (result:any)=>  {
            this.msgs=[];
            this.getTareas();
            this.messageService.add({severity:'success',summary:'Resultado',detail:'Se guardo la Tarea Correctamente.'})
        },error=>{
          console.log(error);
        }
      );
    }
    
  }

  update(tar:Tarea){
    this.tareaService.update(tar).subscribe(
      (result:any)=>{
        console.log(result);
        this.getTareas();
        this.msgs=[];
        this.messageService.add({severity:'success',summary:'Resultado',detail:'Se Actualizo la Tarea Correctamente.'})
      },error=>{
        console.log(error);
      }
    );
  }


  ngOnInit(): void {
      this.getTareas();
      this.cols =[
        { field: 'idTarea', header: 'ID' },
        { field: 'descripcion', header: 'Descripcion' }
      ];
  }

  //Habilita el editar
  onRowEditInit(tarea: Tarea) {
    this.clonedTarea[tarea.id] = {...tarea};
    console.log(this.clonedTarea[tarea.id]);
  }

  //Cancela la edición
  onRowEditCancel(t: Tarea, index: number) {
    this.tareas[index] = this.clonedTarea[t.id];
    delete this.clonedTarea[t.id];
    //this.getTareas();
  }

  //Actualiza los datos de la tabla
  onRowEditSave(tarea: Tarea) {
      this.update(tarea);
  }

  //Elimina la fila seleccionada
  onRowDelete(id:number){
    this.messageService.clear();
    this.messageService.add({key: 'c', sticky: true, severity:'warn', summary:'¿Estas Seguro?', detail:'Eliminar'});
    this.idSelected=id;
  }

  //Confirmación de Eliminación
  onConfirm() {
    console.log(this.idSelected);
    this.messageService.clear('c');
    this.tareaService.delete(this.idSelected).subscribe(
      (result:any)=>{
          console.log(result);
          this.getTareas();
          this.msgs=[];
          this.messageService.add({severity:'success',summary:'Resultado',detail:'Se Elimino la Tarea Correctamente.'})
      },error=>{
          console.log()
      }
    );
  }

  //Cancela la eliminación
  onReject() {
    this.messageService.clear('c');
  }

  SelectRow(tarea:Tarea){
      if(tarea.estado){
        this.tareaService.update(tarea).subscribe(
          (result:any)=>{
            this.msgs=[];
            this.getTareas();
            this.messageService.add({severity:'success',summary:'Estado',detail:'Tarea Finalizada.'})
          },error=>{
            console.log(error);
          }
        );
        
      }else{
        this.tareaService.update(tarea).subscribe(
          (result:any)=>{
            this.msgs=[];
            this.getTareas();
            this.messageService.add({severity:'info',summary:'Estado',detail:'Tarea Pendiente.'})
          },error=>{
            console.log(error);
          }
        );
        
      }

      //console.log(checkValue);
  }
}
